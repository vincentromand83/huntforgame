import React from "react";
import { useState, useContext } from "react";
import GameContext from "../context/GameContext";
import "./gameInfo.css"

function GameInfo() {
  const { games, setGames } = useContext(GameContext);
  const [favorite, setFavorite] = useState(false);
  const displayGame = games && games.length > 0 ? games[0] : {};
  const displayGameGenre = games && games.length > 0 ? games[0].genres.map((genre) => genre.name) : {}
  console.log(displayGameGenre)
  return (
    <div className="container">
      <div className="gameInfo">
        <img className="gameImageInfo" src={displayGame.background_image}></img>
        <h2 class="colorFont">Name : {displayGame.name}</h2>
        <h2 class="colorFont">Release Date : {displayGame.released}</h2>
        <h2 class="colorFont">Genre : {displayGameGenre}</h2>
        <button className="favoriteButton"
          onClick={() => {
            if (favorite) {
              setFavorite(false);
            } else {
              setFavorite(true);
            }
          }}
        >
          {favorite ? "Delete favorite" : "Add to favorite"}
        </button>
      </div>
    </div>
  );
}

export default GameInfo;
