import "./gameList.css";
import { useContext } from "react";
import GameContext from "../context/GameContext";

function GameList() {
  const { games, setGames } = useContext(GameContext);
  console.log(games)
  const displayGameNames = games && games.length > 0 ? games.map((game) => game.name) : {}
  
  return (
    <div className="container">
      {games && games.length > 0 ? games.map((game) => (
        <div className="gameList">
          <h3 className="gameTitle">{game.name}</h3>
          <a href={`/game-info/${game.id}`}>
            <img className="imageGameList" src={game.background_image}></img>
          </a>
        </div>
      )) : ""}
    </div>
  );
}
export default GameList;
