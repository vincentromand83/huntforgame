import "./home.css"

function Home(props) {
  const { title, description } = props;

  return (
    <div className="container">
      <div className="row">
        <h1 className="homeTitle">{title}</h1>
        <div className="homeDescription">{description} </div>
        <button className="homeButton" onClick={() => window.location.href="game-list"}>Afficher la liste de jeux</button>
      </div>
    </div>
  );
}
export default Home;
