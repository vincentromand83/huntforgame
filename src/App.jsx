import { BrowserRouter, Route, Routes } from "react-router-dom";
import React, { useEffect, useState } from "react";
import axios from "axios";
import "./App.css";
import Home from "./component/Home.jsx";
import GameList from "./component/GameList.jsx";
import GameInfo from "./component/GameInfo.jsx";
import GameContext from "./context/GameContext";


function App() {
  const API_URL =
    "https://raw.githubusercontent.com/bastienapp/gamesapi/main/db.json";
  const [games, setGames] = useState({});
  useEffect(() => {
    axios.get(API_URL).then((res) => {
      const games = res.data;
      setGames(games);
    });
  }, []);

  return (
    <div className="App">
      <BrowserRouter>
        <GameContext.Provider value={{ games, setGames }}>
          <Routes>
            <Route
              path="/"
              element={
                <Home
                  title="Hunt for games"
                  description="Avec Hunt for games, 
        trouvez la perle rare dans un océan de jeux vidéos !"
                />
              }
            />
            <Route path="game-list" element={<GameList />} />
            <Route path="game-info" element={<GameInfo />} />
          </Routes>
        </GameContext.Provider>
      </BrowserRouter>
    </div>
  );
}
export default App;
